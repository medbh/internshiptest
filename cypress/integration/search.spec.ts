describe('Search', () => {
    it('should not search if rout is invalid', () => {
      cy.visit('/list')
      cy.url().should('includes','list');
      
    })

    it('should search if input not empty', () => {
        cy.visit('/list')
        cy.url().should('includes','list');
        cy.get('[FormControlName="smth"]').type('chris');
        cy.contains('christopher');
        
      })
  })
  