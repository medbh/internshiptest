describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/list')
    cy.contains('search by author')
    cy.contains('sandbox app is running!')
  })
})
