import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FavoritsComponent } from './components/favorits/favorits.component';
import { PhotoComponent } from './components/photo/photo.component';
import { SearchComponent } from './components/search/search.component';
const routes: Routes = [{path:'',component:DashboardComponent,children:[{path:'list',component:PhotoComponent},{path:'favorits',component:FavoritsComponent},{path:'search',component:SearchComponent}]}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
