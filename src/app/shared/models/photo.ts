
export class Photo {
    id ?: number;
    photo ?: string;
    author ?:string;
    text ? :string;
    favoris?:boolean;
    tmp ?:string;
}