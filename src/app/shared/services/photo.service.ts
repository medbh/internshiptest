import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Photo } from '../models/photo';
import  {map} from 'rxjs/operators'
import { BehaviorSubject } from 'rxjs';
import loremIpsum from 'src/app/shared/loremIpsum';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  photos: Photo[]=[];
  photosFav: Photo[]=[];
  private photosSubject :BehaviorSubject<Photo[]> = new BehaviorSubject(JSON.parse(localStorage.getItem('photo') ) || '[]' );
  private photosFavSubject :BehaviorSubject<Photo[]> = new BehaviorSubject(JSON.parse(localStorage.getItem('photoFav') ) || '[]' );
  httpOptions =  {
    headers : new HttpHeaders({'Content-type' : 'application/json'})
  }


  constructor(private http:HttpClient) { }

  public get photosValue(){
    return this.photosSubject.value;
  }

  setPhotos(photos :Photo[]){
    localStorage.setItem('photo', JSON.stringify(photos));
    this.photosSubject.next(photos);
  
  }


  getphotos(id:number){
    let photo :Photo = {
      id :0,
      photo:"",
      author:"",
      text:""
    };

    return this.http.get('https://picsum.photos/id/'+id+'/info',this.httpOptions)
    .pipe(map((data:any)=>{
    
       photo.id=data.id;
       photo.author=data.author;
       photo.photo='https://picsum.photos/id/'+ id +'/500/500.jpg';
       photo.text=loremIpsum.slice(Math.floor(Math.random() * 6) + 1,300).join(' ');
       photo.favoris=false;
      return photo;
    }));
  }

  setFavorite(p:Photo){

    this.photosFav = this.photosFavSubject.value;

    this.photosFav.push(p);
    localStorage.setItem('photoFav', JSON.stringify(this.photosFav));
    this.photosFavSubject.next(this.photosFav);
  }
  public get photoFavValue(){
    return this.photosFavSubject.value;
  }

  removeFavoris(p:Photo){
    this.photosFav = this.photosFavSubject.value;
    
    const index = this.photosFav.indexOf(p);
    this.photosFav.splice(index,1);
    localStorage.setItem('photoFav', JSON.stringify(this.photosFav));
    this.photosFavSubject.next(this.photosFav);

  }

}
