import { Component, OnInit } from '@angular/core';
import { PhotoService } from 'src/app/shared/services/photo.service';
import { Photo } from 'src/app/shared/models/photo';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  photos:Photo[] = [];
  photosSub:Photo[] = [];
  id=0;
  search = '';
  searchT= '';
  spName="sp1";
  spType= "ball-clip-rotate-multiple";
  constructor(private photoService:PhotoService, private spinner : NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinner.show(this.spName);
    setTimeout(()=>{
      this.spinner.hide(this.spName);

    },4000);

    for(let i = 0;i<1084;i++){ 
          this.photoService.getphotos(i).subscribe(data=> this.photosSub.push(data));
         
    
        }
    this.photos;

  }

  // getPhotos(){
  //   this.photoService.getphotos(this.id).subscribe(data=> this.photos.push(data));
  // }

  checkPhotoExist(p){
    //console.log(this.photoService.photoFavValue.find((o) => o.id === p.id));
    if(!this.photoService.photoFavValue.find((o) => o.id === p.id)) return false;
    return true;
  }
  addFavorit(p:Photo){
    if(!this. checkPhotoExist(p)){
      //p.favoris=true;
      this.photoService.setFavorite(p);

    } else {
      //p.favoris=false;
      this.photoService.removeFavoris(p);
    }
    

  }


  searchOnType(){
    
    this.searchT ='';

     
   }

   tSearchOnType(){
     
       this.search='';
      
   }

   searchPressed(){

    let id:number = + this.search;

    if(this.searchT !=''){
      this.search='';
      this.photos=[];
      this.photos = this.photosSub.filter((res) => {
        
        return (res.author.toLocaleLowerCase().match(this.searchT) || (res.text.match(this.searchT)));
        //return (res.author.toLocaleLowerCase().match(this.searchT));
        
      },);
      

     }
    //  else this.photos=[];

     if(id!=0){
      
      
        this.photoService.getphotos(id).subscribe(data=> this.photos.push(data));  
        this.photos=[];
          
      

    }

   }


}
