import { Component, OnInit } from '@angular/core';
import { Photo } from 'src/app/shared/models/photo';
import { PhotoService } from 'src/app/shared/services/photo.service';

@Component({
  selector: 'app-favorits',
  templateUrl: './favorits.component.html',
  styleUrls: ['./favorits.component.css']
})
export class FavoritsComponent implements OnInit {
  photosFavoris:Photo []= [];

  constructor(private photoService:PhotoService) { }

  ngOnInit(): void {
    this.getPhotoFavoris()
  }

  getPhotoFavoris(){
  
   this.photosFavoris= this.photoService.photoFavValue;
  // console.log(this.photosFavoris) 
}

  deleteFavorit(p:Photo){
    
      p.favoris=false;
      this.photoService.removeFavoris(p);
    

  }
}
