import { Component, OnInit } from '@angular/core';
import { Photo } from 'src/app/shared/models/photo';
import { PhotoService } from 'src/app/shared/services/photo.service';


@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  photos:Photo[] = [];

  photoInit:Photo[]=[];
  scroll =true;
  load =0;
  lastloaded=0;

  number = 0;
  index = 5;
  

  constructor(private photoService:PhotoService) { }

  ngOnInit(): void {
    // if(this.load === 0){
    //   for(let i = this.lastloaded;i<200;i++){ // i should be less than 1083 but for speed matters I made it stop at 200
    //     this.photoService.getphotos(i).subscribe(data=> this.photosSub.push(data));
    //     this.lastloaded++;
    //     if(i>=199) this.load++;
  
    //   }

    //  }
   
  this.getPhotos();
  //console.log(this.photoService.photoFavValue)
  }
  onScroll(){
    if(this.scroll){
      
    //console.log('scrolled !');
    
    while(this.index<500){    //i started directly working on the json file which contain 1084 elements ,so index<1083 rather than 4000

      for(let i =0; i<2 ; i++){
        this.photoService.getphotos(this.index).subscribe(data=> this.photos.push(data)); 
      this.index ++ ;
      }
      this.photoService.setPhotos(this.photos);
      break;
    }
    }
  }

  addFavorit(p:Photo){
    if(!this. checkPhotoExist(p)){
      //p.favoris=true;
      this.photoService.setFavorite(p);

    } else {
      //p.favoris=false;
      this.photoService.removeFavoris(p);
    }
    

  }

  checkPhotoExist(p){
    //console.log(this.photoService.photoFavValue.find((o) => o.id === p.id));
    if(!this.photoService.photoFavValue.find((o) => o.id === p.id)) return false;
    return true;
  }

  getPhotos(){
    
    for(let i = 0;i<5;i++){
      this.photoService.getphotos(i).subscribe(data=> this.photos.push(data));

    }

    this.photoService.setPhotos(this.photos);


   }

   
   



}
